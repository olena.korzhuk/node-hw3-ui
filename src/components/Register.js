import React, {useEffect, useState} from "react";
import {Col, Container, FloatingLabel, Form, NavLink, Row, Toast, ToastContainer} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import {useDispatch, useSelector} from "react-redux";
import {clearState, login, register, userSelector} from "../features/auth/authSlice";
import {useHistory} from "react-router-dom";

const Register = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [role, setRole] = useState('');
    const {isSuccess, isError} = useSelector(userSelector);
    const [toastOpen, setToastOpen] = useState(false);

    const history = useHistory();
    const dispatch = useDispatch();

    const handleEmailInput = (e) => {
        setEmail(e.target.value)
    }

    const handlePasswordInput = (e) => {
        setPassword(e.target.value);
    }

    const handleRoleSelect = (e) => {
        const optionSelected = e.target.options[e.target.selectedIndex].text;
        if (optionSelected !== 'Select your user role') {
            setRole(optionSelected)
        }
    }

    const handleRegister = async (e) => {
        e.preventDefault();
        const userData = {
            email,
            password,
            role
        };
        dispatch(register(userData));
    }

    const handleBackToLoginClick = () => {
        history.push('/login')
    };

    const toggleShow = () => {
        setToastOpen(!toastOpen);
    };

    // useEffect(() => {
    //     return () => {
    //         dispatch(clearState())
    //     }
    // }, [])

    useEffect(() => {
        if (isSuccess) {
            setToastOpen(true);
        }

        if (isError) {
            dispatch(clearState())
        }
    }, [isSuccess, isError]);

    useEffect(() => {
        if (isSuccess && !toastOpen) {
            history.push('/login');
            dispatch(clearState());
        }
    }, [toastOpen]);


    return (
        <Container>
            <Row className="d-flex justify-content-center pt-5">
                <Col xl={4} lg={6} md={8} xs={12} className='bg-light p-5'>
                    <h1 className="mb-5">Sign Up</h1>
                    <Form onSubmit={handleRegister}>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Control
                                type="email"
                                placeholder="Enter email"
                                value={email}
                                onChange={handleEmailInput}/>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Control
                                type="password"
                                placeholder="Password"
                                value={password}
                                onChange={handlePasswordInput}/>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <FloatingLabel controlId="floatingSelect" label="Role">
                                <Form.Select aria-label="Floating label select example" onChange={handleRoleSelect}>
                                    <option>Select your user role</option>
                                    <option value="1">SHIPPER</option>
                                    <option value="2">DRIVER</option>
                                </Form.Select>
                            </FloatingLabel>
                        </Form.Group>

                        <div className="d-flex justify-content-end">
                            <Button variant="primary" type="submit">
                                Register
                            </Button>
                        </div>
                        <div className="form-footer">
                            <NavLink className="p-0" onClick={handleBackToLoginClick}>Go Back to Login</NavLink>
                        </div>
                    </Form>
                </Col>
            </Row>
            <ToastContainer position={"middle-center"} className='my-5'>
                <Toast show={toastOpen} onClose={toggleShow} delay={2000} autohide animation>
                    <Toast.Header>
                        <strong className="me-auto">User profile created successfully</strong>
                    </Toast.Header>
                </Toast>
            </ToastContainer>
        </Container>
    );
}

export default Register;