import React, {useState} from "react";
import {FloatingLabel, Form} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import {useDispatch, useSelector} from "react-redux";
import {userSelector} from "../features/auth/authSlice";
import {clearState, createTruck, fetchTrucks} from "../features/trucks/TrucksSlice";

const AddTruckForm = (props) => {
    const dispatch = useDispatch();
    const {user} = useSelector(userSelector);
    const [type, setType] = useState('');

    const handleTruckSave = (e) => {
        e.preventDefault();
        const truck = {
            type
        };
        const token = user['jwt_token'];
        dispatch(clearState())
        dispatch(createTruck({token, truck}));
        props.hideAddForm();
    };

    const handleCancelAddLoad = () => {
        props.hideAddForm();
    }

    const handleTypeSelect = (e) => {
        const optionSelected = e.target.options[e.target.selectedIndex].text;
        if (optionSelected !== 'Select truck type') {
            setType(optionSelected)
        }
    };

    return (
        <Form className='mb-5 w-50' onSubmit={handleTruckSave}>
            <Form.Group className="mb-3" controlId="formBasicPassword">
                <FloatingLabel controlId="floatingSelect" label="Type">
                    <Form.Select aria-label="Floating label select example" onChange={handleTypeSelect}>
                        <option>Select truck type</option>
                        <option value="1">Sprinter</option>
                        <option value="2">Small Straight</option>
                        <option value="3">Large Straight</option>
                    </Form.Select>
                </FloatingLabel>
            </Form.Group>

            <div className="buttons d-flex justify-content-end">
                <Button variant="secondary" type="submit" onClick={handleCancelAddLoad}>
                    Cancel
                </Button>
                <Button variant="primary" type="submit" className='ms-2'>
                    Submit
                </Button>
            </div>
        </Form>
    )
};

export default AddTruckForm;