import React, {useState} from "react";
import {Form} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import {createLoad, fetchLoads} from "../features/loads/loadsSlice";
import {useDispatch, useSelector} from "react-redux";
import {userSelector} from "../features/auth/authSlice";

const AddLoadForm = (props) => {
    const dispatch = useDispatch();
    const {user} = useSelector(userSelector);
    const [loadName, setLoadName] = useState('');
    const [loadPayload, setLoadPayload] = useState('');
    const [loadDeliveryAddress, setLoadDeliveryAddress] = useState('');
    const [loadPickupAddress, setLoadPickupAddress] = useState('');

    const handleLoadSave = (e) => {
        e.preventDefault();
        const load = {
            loadName,
            loadPayload,
            loadDeliveryAddress,
            loadPickupAddress
        };
        const token = user['jwt_token'];
        dispatch(createLoad({token, load}));
        dispatch(fetchLoads(token));
        props.hideAddForm();
    };

    const handleNameInput = (e) => {
        const name = e.target.value;
        setLoadName(name);
    };

    const handleCancelAddLoad = () => {
        props.hideAddForm();
    }

    const handlePayloadInput = (e) => {
        const payload = e.target.value;
        setLoadPayload(payload);
    };

    const handleDeliveryInput = (e) => {
        const deliveryAddress = e.target.value;
        setLoadDeliveryAddress(deliveryAddress);
    };

    const handlePickupInput = (e) => {
        const pickupAddress = e.target.value;
        setLoadPickupAddress(pickupAddress);
    };

    return (
        <Form className='mb-5 w-50' onSubmit={handleLoadSave}>
            <Form.Group className="mb-3">
                <Form.Label>Name</Form.Label>
                <Form.Control
                    type="text"
                    value={loadName}
                    placeholder="Enter load name"
                    onChange={handleNameInput}/>
            </Form.Group>

            <Form.Group className="mb-3">
                <Form.Label>Approximate Payload</Form.Label>
                <Form.Control
                    type="text"
                    value={loadPayload}
                    placeholder="e.g. 90kg"
                    onChange={handlePayloadInput}
                />
            </Form.Group>

            <Form.Group className="mb-3">
                <Form.Label>Delivery Address</Form.Label>
                <Form.Control
                    type="text"
                    value={loadDeliveryAddress}
                    onChange={handleDeliveryInput}
                    placeholder=""/>
            </Form.Group>

            <Form.Group className="mb-3">
                <Form.Label>Pickup Address</Form.Label>
                <Form.Control
                    type="text"
                    onChange={handlePickupInput}
                    placeholder=""/>
            </Form.Group>

            <div className="buttons d-flex justify-content-end">
                <Button variant="secondary" type="submit" onClick={handleCancelAddLoad}>
                    Cancel
                </Button>
                <Button variant="primary" type="submit" className='ms-2'>
                    Submit
                </Button>
            </div>
        </Form>
    )
};

export default AddLoadForm;