import React from "react";
import Button from "react-bootstrap/Button";

const TruckRow = (props) => {
    const {truck} = props;
    return (
        <tr>
            <td>{props.index}</td>
            <td>{truck.type}</td>
            <td>{truck.assigned_to? truck.assigned_to : '-'}</td>
            <td>{truck.status}</td>
            <td>{truck.created_date}</td>
            <td className='d-flex justify-content-end'>
                <Button
                    variant='danger'
                    className='ms-3'
                    onClick={props.handleDelete}>Delete</Button>
            </td>
        </tr>
    )
};

export default TruckRow;