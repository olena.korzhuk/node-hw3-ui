import React from "react";
import {Container, Nav, Navbar} from "react-bootstrap";
import {Link, useHistory} from "react-router-dom";
import Button from "react-bootstrap/Button";
import {clearState, userSelector} from "../features/auth/authSlice";
import {useDispatch, useSelector} from "react-redux";

const Navigation = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const {user} = useSelector(userSelector);

    const handleLogout = () => {
        localStorage.clear();
        dispatch(clearState());
        history.push('/login');
    };

    return (
        <Navbar bg="light" expand="md" className='mb-3'>
            <Container>
                <div className='d-flex'>
                    <Navbar.Brand as={Link} to="/">Your Logo</Navbar.Brand>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            {/* TODO: make note of this*/}
                            <Nav.Link as={Link} to='/loads'>Loads</Nav.Link>
                            {user.role === 'DRIVER' && <Nav.Link as={Link} to='/trucks'>Trucks</Nav.Link>}
                            {user.role === 'SHIPPER' && <Nav.Link as={Link} to='/posted-loads'>Posted Loads</Nav.Link>}
                            <Nav.Link as={Link} to='/profile'>Profile</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                </div>
                <div>
                    <Button variant="outline-secondary" onClick={handleLogout}>Logout</Button>
                </div>
            </Container>
        </Navbar>
    );
};

export default Navigation;