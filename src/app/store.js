import { configureStore } from '@reduxjs/toolkit';
import {authSlice} from "../features/auth/authSlice";
import {loadsSlice} from "../features/loads/loadsSlice";
import {trucksSlice} from "../features/trucks/TrucksSlice";

const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('state', serializedState);
  } catch (err) {
    console.error(err);
  }
};

export const loadState = () => {
  try {
    const serializedState = localStorage.getItem('state');
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (e) {
    console.error(e);
  }
};

const preloadedState = loadState();

export const store = configureStore({
  reducer: {
    auth: authSlice.reducer,
    loads: loadsSlice.reducer,
    trucks: trucksSlice.reducer
  },
  preloadedState
});

store.subscribe(() => {
  saveState({
    auth: store.getState().auth,
    loads: store.getState().loads,
    trucks: store.getState().trucks
  });
});

