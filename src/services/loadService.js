const getUserLoads = async (authToken) => {
    return await fetch(`/api/loads`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${authToken}`,
            'Content-Type': 'application/json'
        }
    });
};

const createUserLoad = async (token, load) => {
    console.log('NEW LOAD', load);
    const body = JSON.stringify({
        name: load.name,
        payload: load.payload,
        delivery_address: load.loadDeliveryAddress,
        pickup_address: load.loadPickupAddress,
        dimensions: {
            width: 44,
            length: 32,
            height: 66
        }
    });
    console.log('NEW LOAD BODY', body);
    const r = await fetch('/api/loads', {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: load.loadName,
            payload: load.loadPayload,
            delivery_address: load.loadDeliveryAddress,
            pickup_address: load.loadPickupAddress,
            dimensions: {
                width: 44,
                length: 32,
                height: 66
            }
        })
    });
    console.log(r);
    return r;
};

export default {
    getUserLoads,
    createUserLoad
};