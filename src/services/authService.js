import jwt from 'jsonwebtoken';

const loginUser = async (email, password) => {
    return await fetch(`/api/auth/login`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email: email,
            password: password
        })
    });
};

const registerUser = async (email, password, role) => {
    return await fetch('/api/auth/register', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email: email,
            password: password,
            role: role
        })
    });
};

const getTokenPayload = (token) => {
    const secret = 'my-jwt-secret-key';
    const verified = jwt.verify(token, secret);
    return {id: verified.id, username: verified.username, role: verified.role};
};


export default {
    loginUser,
    registerUser,
    getTokenPayload
};