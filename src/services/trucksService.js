const getTrucks = async (authToken) => {
    return await fetch(`/api/trucks`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${authToken}`,
            'Content-Type': 'application/json'
        }
    });
};

const createUserTruck = async (token, truck) => {
    const body = JSON.stringify({
        type: truck.type.toUpperCase()
    });
    const r = await fetch('/api/trucks', {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        },
        body: body
    });
    return r;
};

const deleteUserTruck = async (token, truckId) => {
    const r = await fetch(`/api/trucks/${truckId}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        },
    });
    return r;
}

export default {
    getTrucks,
    createUserTruck,
    deleteUserTruck
};