import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route} from "react-router-dom";
import Login from "./components/Login";
import Home from "./pages/Home";
import {Container} from "react-bootstrap";
import Register from "./components/Register";
import PrivateRoute from "./features/PrivateRoute";
import Loads from "./features/loads/Loads";
import {Switch} from "react-router";
import PostedLoads from "./components/PostedLoads";
import Trucks from "./features/trucks/Trucks";

function App() {
    return (
        <Router>
            <Container className="p-3">
                <Switch>
                    <Route exact path="/register" component={Register}/>
                    <Route exact path="/login" component={Login}/>
                    <PrivateRoute exact path="/loads" component={Loads}/>
                    <PrivateRoute exact path="/trucks" component={Trucks}/>
                    <PrivateRoute exact path="/posted-loads" component={PostedLoads}/>
                    <PrivateRoute exact path="/" component={Home}/>
                </Switch>
            </Container>
        </Router>
    );
}

export default App;
