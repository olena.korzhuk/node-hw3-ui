import React from "react";
import {Redirect, Route} from "react-router";
import {useSelector} from "react-redux";
import {userSelector} from "./auth/authSlice";
import Navigation from "../components/Navigation";

const PrivateRoute = ({component: Component, ...restOfProps}) => {
    const {user} = useSelector(userSelector);

    return (
        <Route
            {...restOfProps}
            render={(props) =>
                user ? (
                    <>
                        <Navigation/>
                        <Component {...props} />
                    </>
                ) : <Redirect to="/login"/>
            }
        />
    );
}

export default PrivateRoute;
