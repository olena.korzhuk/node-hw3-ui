import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import authService from "../../services/authService";

const initialState = {
    user: null,
    isFetching: false,
    isSuccess: false,
    isError: false
};

export const login = createAsyncThunk(
    'login',
    async ({email, password}, thunkAPI) => {
        try {
            const response = await authService.loginUser(email, password);
            if (response.ok) {
                const responseParsed = await response.json();
                const {role} = authService.getTokenPayload(responseParsed['jwt_token']);
                return {...responseParsed, role};
            } else {
                const responseParsed = await response.json();
                return thunkAPI.rejectWithValue(responseParsed);
            }

        } catch (error) {
            thunkAPI.rejectWithValue(error.response.data)
        }
    }
);

export const register = createAsyncThunk(
    'register',
    async ({email, password, role}, thunkAPI) => {
        try {
            const response = await authService.registerUser(email, password, role);
            if (response.ok) {
                const responseParsed = await response.json();
                return {
                    user: {
                        email,
                        password,
                        role
                    },
                    message: responseParsed.message
                };
            } else {
                const responseParsed = await response.json();
                return thunkAPI.rejectWithValue(responseParsed);
            }
        } catch (error) {
            thunkAPI.rejectWithValue(error.response.data)
        }
    }
);

// export const logout = createAsyncThunk(
//     'logout',
//     async (_, thunkAPI) => {
//         try {
//
//         } catch (error) {
//             return thunkAPI.rejectWithValue({error: error.message});
//         }
//     }
// );

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        clearState: (state) => {
            state.isError = false;
            state.isSuccess = false;
            state.isFetching = false;
        }
    },
    extraReducers: (builder) => {
        builder.addCase(login.fulfilled, (state, {payload}) => {
            state.user = payload;
            state.isFetching = false;
            state.isSuccess = true;
        });
        builder.addCase(login.rejected, (state, {payload}) => {
            state.isError = true;
            state.isFetching = false;
            state.error = payload.message;
        });
        builder.addCase(login.pending, (state, action) => {
            state.isFetching = true;
        });
        builder.addCase(register.fulfilled, (state, {payload}) => {
            state.user = payload.user;
            state.isFetching = false;
            state.isSuccess = true;
        });
        builder.addCase(register.rejected, (state, {payload}) => {
            state.isError = true;
            state.error = payload;
            state.isFetching = false;
        });
        builder.addCase(register.pending, (state, action) => {
            state.isFetching = true;
        });
    }
});

export const {clearState} = authSlice.actions;

export const userSelector = (state) => state.auth;