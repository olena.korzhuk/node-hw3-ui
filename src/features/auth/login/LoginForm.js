import React, {useEffect, useState} from "react";
import {Col, Container, Form, NavLink, Row, Toast, ToastContainer} from "react-bootstrap";
import Button from 'react-bootstrap/Button';
import {useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {login, clearState, userSelector} from "../authSlice";

const LoginForm = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const {user, isSuccess, isError, error} = useSelector(userSelector);
    const [toastOpen, setToastOpen] = useState(false);


    const history = useHistory();
    const dispatch = useDispatch();

    const toggleShow = () => {
        setToastOpen(!toastOpen);
    };

    const handleEmailInput = (e) => {
        setEmail(e.target.value)
    }

    const handlePasswordInput = (e) => {
        setPassword(e.target.value);
    }

    const handleClick = () => {
        history.push('/register');
    };

    const handleLogin = async (e) => {
        e.preventDefault();
        const userData = {
            email,
            password,
        };
        dispatch(login(userData));
    }

    useEffect(() => {
        if (user) {
            setEmail(user.email);
        }
        return () => {
            dispatch(clearState())
        }
    }, [])

    useEffect(() => {
        if (isSuccess) {
            history.push('/')
        }

        if (isError) {
            dispatch(clearState());
            setToastOpen(true);
        }
    }, [isSuccess, isError]);

    return (
        <Container>
            <Row className="d-flex justify-content-center pt-5">
                <Col xl={4} lg={6} md={8} xs={12} className='bg-light p-5'>
                    <h1 className="mb-5">Sign In</h1>
                    <Form onSubmit={handleLogin}>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Control
                                type="email"
                                placeholder="Enter email"
                                value={email}
                                onChange={handleEmailInput}/>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Control
                                type="password"
                                placeholder="Password"
                                value={password}
                                onChange={handlePasswordInput}/>
                        </Form.Group>
                        <div className="d-flex justify-content-end">
                            <Button variant="primary" type="submit">
                                Login
                            </Button>
                        </div>
                        <div className="form-footer">
                            <p>New to application?</p>
                            <NavLink className="p-0" onClick={handleClick}>Sign Up Now</NavLink>
                        </div>
                    </Form>
                </Col>
            </Row>
            <ToastContainer position={"bottom-center"} className='my-5' >
                <Toast show={toastOpen} onClose={toggleShow} delay={2000} autohide animation className='bg-danger'>
                    <Toast.Header>
                        <strong className="me-auto">{error}</strong>
                    </Toast.Header>
                </Toast>
            </ToastContainer>
        </Container>
    );
};

export default LoginForm;


