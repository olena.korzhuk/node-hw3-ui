import React, {useEffect, useState} from "react";
import {Table, Toast, ToastContainer} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {userSelector} from "../auth/authSlice";
import Button from "react-bootstrap/Button";
import {clearState, deleteTruck, fetchTrucks, trucksSelector} from "./TrucksSlice";
import AddTruckForm from "../../components/AddTruckForm";
import TruckRow from "../../components/TruckRow";
import {useHistory} from "react-router-dom";

const table_headings = ['TRUCK TYPE', 'ASSIGNED TO', 'STATUS', 'CREATED DATE']

const Trucks = () => {
    const dispatch = useDispatch();
    const {user} = useSelector(userSelector);
    const {trucks, isSuccess, isError, message} = useSelector(trucksSelector);
    const [addFormShow, setAddFormShow] = useState(false);
    const [toastOpen, setToastOpen] = useState(false);

    const toggleShow = () => {
        setToastOpen(!toastOpen);
    };

    useEffect(() => {
        const token = user['jwt_token']
        dispatch(fetchTrucks(token));
    }, [isSuccess, isError]);

    const openAddForm = () => {
        setAddFormShow(true);
    };

    const closeAddForm = () => {
        setAddFormShow(false);
    };

    const handleDelete = (truckId) => {
        const token = user['jwt_token'];
        dispatch(clearState());
        dispatch(deleteTruck({token, truckId}));
        setToastOpen(true);
    }

    return trucks && (
        <div className='mt-5'>
            {addFormShow && (
                <AddTruckForm hideAddForm={closeAddForm} showAddForm={openAddForm}/>
            )}
            <div className='container'>
                <div className='d-flex justify-content-between mb-3'>
                    <h1 className='mb-3'>Trucks</h1>
                    <div className='align-self-center'>
                        <Button variant="primary" onClick={openAddForm}>Add Truck</Button>
                    </div>
                </div>
                {trucks.length
                    ? <Table responsive className='border-0'>
                        <thead>
                        <tr>
                            <th>#</th>
                            {Array.from({length: table_headings.length}).map((_, index) => (
                                <th key={index}>{table_headings[index]}</th>
                            ))}
                        </tr>
                        </thead>
                        <tbody>
                        {trucks.map((truck, index) => {
                            return (
                                <TruckRow
                                    key={truck._id}
                                    id={truck._id}
                                    truck={truck}
                                    index={++index}
                                    handleDelete={() => handleDelete(truck._id)}/>
                            )
                        })}
                        </tbody>
                    </Table>
                    : <p className='text-center'>No trucks to show</p>
                }
            </div>
            <ToastContainer position={"bottom-center"} className='my-5' >
                <Toast show={toastOpen} onClose={toggleShow} delay={1500} autohide animation className='bg-danger'>
                    <Toast.Header>
                        <strong className="me-auto">{message}</strong>
                    </Toast.Header>
                </Toast>
            </ToastContainer>
        </div>
    );
};

export default Trucks;