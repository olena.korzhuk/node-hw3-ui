import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import truckService from "../../services/trucksService";

const initialState = {
    trucks: [],
    isFetching: false,
    isSuccess: false,
    isError: false,
    message: ''
};

export const fetchTrucks = createAsyncThunk(
    'fetchTrucks',
    async (token, thunkAPI) => {
        try {
            const response = await truckService.getTrucks(token);
            if (response.ok) {
                return await response.json();
            } else {
                const responseParsed = await response.json();
                return thunkAPI.rejectWithValue(responseParsed);
            }
        } catch (error) {
            thunkAPI.rejectWithValue(error.response.data)
        }
    }
);

export const createTruck = createAsyncThunk(
    'createTruck',
    async ({token, truck}, thunkAPI) => {
        try {
            const response = await truckService.createUserTruck(token, truck);
            if (response.ok) {
                return await response.json();
            } else {
                const responseParsed = await response.json();
                return thunkAPI.rejectWithValue(responseParsed);
            }
        } catch (error) {
            thunkAPI.rejectWithValue(error.response.data)
        }
    }
);

export const deleteTruck = createAsyncThunk(
    'deleteTruck',
    async ({token, truckId}, thunkAPI) => {
        try {
            const response = await truckService.deleteUserTruck(token, truckId);
            if (response.ok) {
                return await response.json();
            } else {
                const responseParsed = await response.json();
                return thunkAPI.rejectWithValue(responseParsed);
            }
        } catch (error) {
            thunkAPI.rejectWithValue(error.response.data)
        }
    }
);

export const trucksSlice = createSlice({
    name: 'trucks',
    initialState,
    reducers: {
        clearState: (state) => {
            state.isError = false;
            state.isSuccess = false;
            state.isFetching = false;
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchTrucks.fulfilled, (state, {payload}) => {
                state.trucks = payload.trucks;
                state.isFetching = false;
                state.isSuccess = true;
            })
            .addCase(fetchTrucks.rejected, (state, {payload}) => {
                state.isError = true;
                state.isFetching = false;
            })
            .addCase(fetchTrucks.pending, (state, action) => {
                state.isFetching = true;
            })
            .addCase(createTruck.fulfilled, (state, {payload}) => {
                state.isFetching = false;
                state.isSuccess = true;
            })
            .addCase(createTruck.rejected, (state, {payload}) => {
                state.isError = true;
                state.isFetching = false;
            })
            .addCase(createTruck.pending, (state, action) => {
                state.isFetching = true;
            })
            .addCase(deleteTruck.fulfilled, (state,{payload}) => {
                state.isFetching = false;
                state.isSuccess = true;
                state.message = payload.message;
            })
            .addCase(deleteTruck.pending, (state) => {
                state.isFetching = true;
            })
            .addCase(deleteTruck.rejected, (state, {payload}) => {
                state.isError = true;
                state.isFetching = false;
                state.message = payload.message;
            })
    }
});

export const {clearState} = trucksSlice.actions;

export const trucksSelector = (state) => state.trucks;