import React, {useEffect, useState} from "react";
import {userSelector} from "../auth/authSlice";
import {Form, Table} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {fetchLoads, loadsSelector, createLoad} from "./loadsSlice";
import Button from "react-bootstrap/Button";
import AddLoadForm from "../../components/AddLoadForm";

const table_headings = ['LOAD NAME', 'CREATED DATE', 'PICK-UP ADDRESS', 'DELIVERY ADDRESS']

const Loads = () => {
    const dispatch = useDispatch();
    const {user} = useSelector(userSelector);
    const {loads} = useSelector(loadsSelector);
    const [addFormShow, setAddFormShow] = useState(false);

    useEffect(() => {
        const token = user['jwt_token']
        dispatch(fetchLoads(token));
    }, []);

    const openAddForm = () => {
        setAddFormShow(true);
    };

    const closeAddForm = () => {
        setAddFormShow(false);
    };

    return loads && (
        <>
            {addFormShow && (
                <AddLoadForm hideAddForm={closeAddForm} showAddForm={openAddForm}/>
            )}
            <div className='container'>
                <div className='d-flex justify-content-between mb-3'>
                    <h1 className='mb-3'>Loads</h1>
                    <div className='align-self-center'>
                        {user.role === 'SHIPPER' && <Button variant="primary" onClick={openAddForm}>Add Load</Button>}
                    </div>
                </div>
                {loads.length
                    ? <Table responsive>
                        <thead>
                        <tr>
                            <th>#</th>
                            {Array.from({length: table_headings.length}).map((_, index) => (
                                <th key={index}>{table_headings[index]}</th>
                            ))}
                        </tr>
                        </thead>
                        <tbody>
                        {loads.map((load, index) => {
                            return (<tr>
                                <td>{++index}</td>
                                <td>{load.name}</td>
                                <td>{load.created_date}</td>
                                <td>{load.pickup_address}</td>
                                <td>{load.delivery_address}</td>
                                <td>Edit</td>
                                <td>Post</td>
                            </tr>)
                        })}
                        </tbody>
                    </Table>
                    : <p>No loads to show</p>
                }
            </div>
        </>
    );
}

export default Loads;