import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import loadService from "../../services/loadService";

const initialState = {
    loads: [],
    isFetching: false,
    isSuccess: false,
    isError: false
};

export const fetchLoads = createAsyncThunk(
    'fetchLoads',
    async (token, thunkAPI) => {
        try {
            const response = await loadService.getUserLoads(token);
            if (response.ok) {
                return await response.json();
            } else {
                const responseParsed = await response.json();
                return thunkAPI.rejectWithValue(responseParsed);
            }
        } catch (error) {
            thunkAPI.rejectWithValue(error.response.data)
        }
    }
);

export const createLoad = createAsyncThunk(
    'createLoad',
    async ({token, load}, thunkAPI) => {
        try {
            const response = await loadService.createUserLoad(token, load);
            if (response.ok) {
                return await response.json();
            } else {
                const responseParsed = await response.json();
                return thunkAPI.rejectWithValue(responseParsed);
            }
        } catch (error) {
            thunkAPI.rejectWithValue(error.response.data)
        }
    }
);

export const loadsSlice = createSlice({
    name: 'loads',
    initialState,
    reducers: {
        clearState: (state) => {
            state.isError = false;
            state.isSuccess = false;
            state.isFetching = false;
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchLoads.fulfilled, (state, {payload}) => {
                state.trucks = payload.trucks;
                state.isFetching = false;
                state.isSuccess = true;
            })
            .addCase(fetchLoads.rejected, (state, {payload}) => {
                state.isError = true;
                state.isFetching = false;
            })
            .addCase(fetchLoads.pending, (state, action) => {
                state.isFetching = true;
            })
            .addCase(createLoad.fulfilled, (state, {payload}) => {
                state.isFetching = false;
                state.isSuccess = true;
            })
            .addCase(createLoad.rejected, (state, {payload}) => {
                state.isError = true;
                state.isFetching = false;
            })
            .addCase(createLoad.pending, (state, action) => {
                state.isFetching = true;
            });
    }
});

export const {clearState} = loadsSlice.actions;

export const loadsSelector = (state) => state.loads;